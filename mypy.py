from p4p.client.thread import Context
from p4p import Type, Value
from functools import partial
import time

Context.providers()
['pva']
contex = Context('pva')
ctxt = Context('pva', nt=False)
#ctxt = Context('pva')

#V = contex.get('nh-cnt')
V = contex.get('YMIR-TS:Ctrl-EVR-02:10-TS-I')
#print(V.raw)
pvgetValue = ctxt.get('YMIR-TS:Ctrl-EVR-02:10-TS-I')
#print('pvget: ', pvgetValue['timeStamp']['nanoseconds'])


#def cb(V):
#       print('New value', V)
#sub = ctxt.monitor('nh-cnt', cb)
def myCallBack(pvname, pvmonValue):
       nanosecs = str((pvmonValue['timeStamp']['nanoseconds']))
       print('Name: ', pvname, 'Value(s): ',pvmonValue['value'], 'No of elmnts: ', len(pvmonValue['value']), ' timestamp:{}{}'.format(pvmonValue['timeStamp']['secondsPastEpoch'],nanosecs.zfill(9)))

pvs = ['YMIR-TS:Ctrl-EVR-02:00-TS-I', 'YMIR-TS:Ctrl-EVR-02:10-TS-I']
subscriptions = []

for pv in pvs:
       subscriptions.append(ctxt.monitor(pv, partial(myCallBack,pv)))

time.sleep(2) # wait seconds to close monitor
for sub in subscriptions:
	sub.close()

